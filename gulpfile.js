var gulp = require('gulp');
var concat = require('gulp-concat');
var minify = require('gulp-minify');
var cleanCss = require('gulp-clean-css');
var rev = require('gulp-rev');
var del = require('del');
var inject = require('gulp-inject');

let paths = {
    index : '*.html',
};

gulp.task('clean-js', function () {
    return del([
        'public/build/js/*.js'
    ]);
});

gulp.task('clean-css', function () {
    return del([
        'public/build/css/*.css'
    ]);
});

gulp.task('pack-js', ['clean-js'], function () {
    return gulp.src([
        'assets/js/jquery-2.1.3.min.js',
        'assets/js/modernizr.custom.js',
        'assets/js/bootstrap.min.js',
        'assets/js/vue.min.js',
        'assets/js/page-transition.js',
        'assets/js/imagesloaded.pkgd.min.js',
        'assets/js/validator.js',
        'assets/js/jquery.shuffle.min.js',
        'assets/js/masonry.pkgd.min.js',
        'assets/js/owl.carousel.min.js',
        'assets/js/jquery.magnific-popup.min.js',
        'assets/js/jquery.hoverdir.js',
        'assets/js/main.js',
        'assets/js/axios.min.js'
    ])
        .pipe(concat('ugochuwku-production.js'))
        .pipe(minify({
            ext:{
                min:'.js'
            },
            noSource: true
        }))
        .pipe(rev())
        .pipe(gulp.dest('public/build/js'))
        .pipe(rev.manifest('public/build/ugochukwu-manifest.json', {
            merge: true
        }))
        .pipe(gulp.dest(''));
});

gulp.task('pack-css', ['clean-css'], function () {
    return gulp.src([
        'assets/css/bootstrap.min.css',
        'assets/css/normalize.css',
        'assets/css/transition-animations.css',
        'assets/css/owl.carousel.css',
        'assets/css/magnific-popup.css',
        'assets/css/animate.css',
        'assets/css/main.css'
    ])
        .pipe(concat('ugochukwu-production.css'))
        .pipe(cleanCss())
        .pipe(rev())
        .pipe(gulp.dest('public/build/css'))
        .pipe(rev.manifest('public/build/ugochukwu-manifest.json', {
            merge: true
        }))
        .pipe(gulp.dest(''));
});

gulp.task('inject-files', () => {
    gulp.src(paths.index)
        .pipe(inject(gulp.src(['public/build/js/*.js', 'public/build/css/*.css'], {read: false} ), { addRootSlash : false } ))
        .pipe(gulp.dest('./'));
});

gulp.task('watch', function() {
    gulp.watch('assets/js/**/*.js', ['pack-js']);
    gulp.watch('assets/css/**/*.css', ['pack-css']);
    gulp.watch(['public/build/js/*.js', 'public/build/css/*.css'], ['inject-files']);
});

gulp.task('default', ['watch']);