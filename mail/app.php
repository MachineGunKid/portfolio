<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");

require '../vendor/autoload.php';

$rest_json = file_get_contents("php://input");
$_POST = json_decode($rest_json, true);

if (empty($_POST['name']) && empty($_POST['email']) && empty($_POST['message'])) {
    echo json_encode([
        "status" => false,
        "message" => 'All fields are required'
    ], 200);
}

$name = $_POST['name'];
$to = "phalcon.vee@gmail.com";
$from = $_POST['email'];
$message = $_POST['message'];

$email = new \SendGrid\Mail\Mail();
$email->setFrom($from, $name);
$email->setSubject("My Portfolio Inquiry Form");
$email->addTo($to);
$email->addContent("text/plain", $message);
$email->addContent(
    "text/html", $message
);

$apiKey = "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";

$sendgrid = new \SendGrid($apiKey);
try {
    $response = $sendgrid->send($email);
    echo json_encode(array(
        "status" => true,
        "response_code" => $response->statusCode(),
        "headers" => $response->headers(),
        "body" =>  $response->body()
    ));

} catch (Exception $e) {
    echo json_encode([
        "status" => false,
        "message" => $e->getMessage()
    ]);
}
